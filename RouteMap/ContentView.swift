//
//  ContentView.swift
//  RouteMap
//
//  Created by Администратор on 11.12.2020.
//

import SwiftUI
import MapKit

struct ContentView: View {
    var body: some View {
        mapView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct mapView: UIViewRepresentable {
    func makeCoordinator() -> Coordinator {
        return mapView.Coordinator()
    }
    

    func makeUIView(context: Context) -> MKMapView {
        let map = MKMapView()
        let sourceCoordinate = CLLocationCoordinate2D(latitude: 52, longitude: 37)
        let destinationCoordinate = CLLocationCoordinate2D(latitude: 52.2, longitude: 37)
        let region = MKCoordinateRegion(center: sourceCoordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        
        
        
        let pin = MKPointAnnotation()
        pin.coordinate = sourceCoordinate
        map.addAnnotation(pin)
        
        let pinTwo = MKPointAnnotation()
        pinTwo.coordinate = destinationCoordinate
        map.addAnnotation(pinTwo)
        
        let pinThree = MKPointAnnotation()
        pinThree.coordinate = CLLocationCoordinate2D(latitude: 55, longitude: 38.1)
        map.addAnnotation(pinThree)
        
        map.setRegion(region, animated: true)
        map.delegate = context.coordinator
        
//        let request = MKDirections.Request()
//        request.source = MKMapItem(placemark: MKPlacemark(coordinate: sourceCoordinate))
//        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate))
//
//        let direction = MKDirections(request: request)
//
//        direction.calculate { (response, error) in
//            if error != nil {
//                print(error?.localizedDescription)
//                return
//            }
//
//            let route = response!.routes.first
//            map.addOverlay(route!.polyline)
//        }
        
        return map
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .orange
            renderer.lineWidth = 5
            return renderer
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            mapView.removeOverlays(mapView.overlays)
            drawRoute(dest: view.annotation!.coordinate, map: mapView)
        }
        
        func drawRoute(dest: CLLocationCoordinate2D, map: MKMapView) {
            let request = MKDirections.Request()
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 52, longitude: 37)))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: dest))
            
            let direction = MKDirections(request: request)
            
            direction.calculate { (response, error) in
                if error != nil {
                    print(error?.localizedDescription)
                    return
                }
                
                for route in response!.routes {
                    map.addOverlay(route.polyline)
                }
            }
        }
    }
}
